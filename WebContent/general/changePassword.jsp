<%@page import="com.attendance.Details"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Change Password</title>
<script type="text/javascript">
	function validateForm() {
		var newPass = document.forms["myForm"]["newPass"].value;
		var newPassAgain = document.forms["myForm"]["ReNewPass"].value;
		if (newPass != newPassAgain) {
			alert("Password didnt match");
			return false;
		} else {
			return confirmation();
		}
	}

	function confirmation() {
		var answer = confirm("Are you Sure?");
		if (answer) {
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>

	<center>
		<table border=2 cellspacing=0
			style="margin: 10px; padding: 10px; width: auto; height: auto;">
			<%
				boolean showTable = true;
				boolean alreadyChanged = false;
				if (session.getAttribute("username") == null) {
			%>
			<jsp:include page="failedMessage.jsp"></jsp:include>
			<%
				} else {

					String username = session.getAttribute("username").toString();
					String oldPass = request.getParameter("oldPass");
					String newPass = request.getParameter("newPass");
					Details details = new Details();
					if (username != null && newPass != null) {
						if (details.changePassword(username, oldPass, newPass)) {
							showTable = true;
							alreadyChanged = true;
			%>

			<tr style="color: #70CD37; background-color: #339966; font: bold;">
				<td colspan="2"><center>Successfully Changed</center></td>
			</tr>

			<%
				} else {
							showTable = true;
			%>
			<tr style="color: #E95145; background-color: #339966; font: bold;">
				<td colspan="2"><center>Couldn't Change</center></td>
			</tr>
			<%
				}
					}

					if (showTable) {
			%>

			<form method="post" name="myForm" onSubmit="return validateForm()"
				action="changePassword.jsp">
				<tr style="color: #F3F9F6; background-color: #339966; font: bold;">
					<td colspan=2><center>Change Password</center></td>
				</tr>
				<tr style="color: black; background-color: #CCFFCC;">
					<td>Old Password</td>
					<td><input type="password" name="oldPass" /></td>
				<tr style="color: black; background-color: #CCFFCC;">
					<td>New Password</td>
					<td><input type="password" name="newPass" /></td>
				<tr>
				<tr style="color: black; background-color: #CCFFCC;">
					<td>Retype New Password</td>
					<td><input type="password" name="ReNewPass" /></td>
				<tr style="color: black; background-color: #CCFFCC;">
					<td colspan="2"><center>
							<input type="submit"
								value="<%if (alreadyChanged)
						out.println("Change Again");
					else
						out.println("Change");%>" />
						</center></td>
				</tr>

			</form>
		</table>

	</center>
	<%
		}
		}
	%>

</body>
</html>