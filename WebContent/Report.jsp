<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Administration Panel</h1></center>

	<%
		if (session.getAttribute("username") == null) {
	%>
		<h1 style="color: #F3F9F6; background-color: #339966; font: bold;">Something Weird Happened<br>
		</h1>

	<%
		} else {
	%>
	<base target="content">
	</br>
		<table border =2 cellspacing =0 style="margin:10px; padding:10px; width: auto; height:auto; ">
		<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;">
		<td>Report</td>
		</tr>
						<tr style="color:black; background-color:#CCFFCC ;">
						<td>
			<a href="reports/WeeklyReport.jsp" target="content">Weekly
				Report</a>
		</td>
						<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="reports/CustomReport.jsp" target="content">Custom
				Report</a>
				</td>
				</tr>
		
				<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;">
		
				<td>
				Settings</td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/viewUsers.jsp" target="content">View All Users
			</a>
			</td></tr>
		
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/addUser.jsp" target="content">Add User</a></td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/deleteUser.jsp" target="content">Remove User</a></td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/alterUserInfo.jsp" target="content">Modify User
				Information</a></td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/viewAdmins.jsp" target="content">View All
				Administrators </a></td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/addAdmin.jsp" target="content">Add Admin</a></td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/removeAdmin.jsp" target="content">Remove Admin</a></td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/modifyParameters.jsp" target="content">Modify Parameters</a></td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/changePassword.jsp" target="content">Change
				Password</a></td></tr>
		<tr style="color:black; background-color:#CCFFCC ;">
						<td><a href="general/logout.jsp" target="content">Logout</a></td></tr>
		</table>
	<%
		}
	%>
</body>
</html>