<%@page import="java.util.HashMap"%>
<%@page import="com.attendance.Details"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">

function confirmation() {
	var answer = confirm("Are you Sure?");
	if (answer){
		return true;
	}
	else{
		return false;
	}
}


</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>

	<center>
		<table border=2 cellspacing=0
			style="margin: 10px; padding: 10px; width: auto; height: auto;">
			<%
				if (session.getAttribute("username") == null) {
			%>
			<jsp:include page="failedMessage.jsp"></jsp:include>
			<%
				} else {

					boolean removedOnce = false;
					Details details = new Details();

					String[] ip_address = request.getParameterValues("user");
					if (ip_address != null) {

						boolean result = details.removeUsers(ip_address);
						if (result) {
			%>
			<tr style="color: #70CD37; background-color: #339966; font: bold;">
				<td colspan="3"><center>Successfully Removed</center></td>
			</tr>
			<%
				removedOnce = true;
						} else {
			%>
			<tr style="color: #E95145; background-color: #339966; font: bold;">
				<td colspan="3"><center>Failed To Remove</center></td>
			</tr>
			<%
				}
					}

					List<HashMap<String, String>> mapList = details
							.getAllUsersAndIpAddress();
					if (mapList == null) {
						//Handle accordinglyt
					} else {
			%>

			<tr style="color: #F3F9F6; background-color: #339966; font: bold;">

				<td colspan=3><center>Select To Remove Users</center></td>
			</tr>
			<form method="post" action="deleteUser.jsp" onsubmit="return confirmation();">
				<tr style="color: #F3F9F6; background-color: #339966; font: bold;">
					<td>Select</td>
					<td>User Name</td>
					<td>User IP address</td>
				</tr>
			<%
				for (HashMap<String, String> map : mapList) {
			%>
			<tr style="color: black; background-color: #CCFFCC;">
				<td><input type="checkbox" name="user"
					value="<%=map.get("ipAddress")%>" /></td>
				<td><%=map.get("name")%></td>
				<td><%=map.get("ipAddress")%></td>


			</tr>

			<%
				}
			%>
			<tr style="color: black; background-color: #CCFFCC;">
				<td colspan="3"><center>
						<input type="submit"
							value="<%if (removedOnce)
						out.println("Delete Again");
					else
						out.println("Delete");%>" />
					</center></td>
			</tr>

			</form>
		</table>


	</center>

	<%
		}
		}
	%>
</body>
</html>