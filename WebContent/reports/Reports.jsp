<%@page import="com.attendance.Details,java.*,javax.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Weekly Attendance</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>


	<%
	if(session.getAttribute("username") == null){
		
		%>
				<jsp:include page="../general/failedMessage.jsp"></jsp:include>
				<%	
	}else{
		//get these parameters from previous page through request parameter
			String wk = request.getParameter("week");
			String mth = request.getParameter("month");
			String yr = request.getParameter("year");

			int week = Integer.parseInt(wk);
			int month = Integer.parseInt(mth) - 1;
			int year = Integer.parseInt(yr);

			Details details = new Details();
			if (week > 5 || week < 1) {
		out.println("Week value ranges from 1 to 5 only <br> ");
			} else if (month > 11 || month < 0) {
		out.println("Month value ranges from 1 to 12 only <br>");
			} else if (details.doesExistsRecord(week, month, year) == false) {
		out.println("No records  of specified time in the database <br>");
			} else {

		details.setWeekAllValues();

		int check = 8; //to check if week has few days only
	%>

	<center>
		<h1 style="color: #F3F9F6; background-color: #339966; font: bold;">
			Attendance of
			<%=week%>
			<sup>th</sup> week of
			<%=month + 1%>-<%=year%></h1>
		<table border=2 cellspacing=0
			style="margin: 10px; padding: 10px; width: auto; height: auto;">

			<tr style="color: #F3F9F6; background-color: #339966; font: bold;">
				<td>Name</td>
				<td>Info</td>
				<%
					//to display date in the first column
								for (int i = 0; i < details.weekDays.length; i++) {
									if (details.weekDays[i] == null) {
										check = i + 1; //after i days week ends
										break;
									}
				%>
				<td><%=details.weekDays[i]%></td>
				<%
					}
				%>


			</tr>

			<%
				for (int i = 0; i < details.noOfUsers; i++) {

						if (i % 2 == 0) {
			%>
			<tr style="color:black; background-color:#CCFFCC ;">
				<%
					} else {
				%>
			
			<tr style="background-color: gray;">
				<%
					}
							for (int j = -2; j < check - 1; j++) { //use j<check instead   //j is initialized to -1 to fit the table(table contains extra column in the left)
								//if(j==3)break;			//j<check-1 coz we should decrease because of above reason
								if (j == -2) {
				%>
				<td><%=details.userName[i]%></td>
				<%
					//since 0th term of array stores  the name of the respective user 
									continue;

								} else if (j == -1) {
				%>
				<td>IN<br>OUT
				</td>
				<%
					continue;
								}

								if (details.status[i][j] == "0"
										|| details.status[i][j] == null) {
				%>
				<td><%="<center>_</center>"%></td>
				<%
					} else {
				%>
				<td>
					<%
						String loginValue, logoutValue;
										if (details.logintime[i][j] == null
												&& details.logouttime[i][j] == null) {
											loginValue = "-";
										} else {
											if (details.logintime[i][j] == null)
												loginValue = "<center>_</center>";
											else
												loginValue = details.logintime[i][j];
											if (details.logouttime[i][j] == null)
												logoutValue = "<center>_</center>";
											else
												logoutValue = details.logouttime[i][j];
					%> <%=loginValue%> <%="\n"%> <br> <%=logoutValue%>
				</td>
				<%
					}

								}
							}
				%>
			</tr>
			<%
				}
				}
	}
			%>



		</table>
		
	</center>

</body>
</html>