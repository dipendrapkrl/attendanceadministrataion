<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.attendance.Details"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	function getIpAddress() {

		var a = document.forms["mform"]["ipFieldA"].value;
		var b = document.forms["mform"]["ipFieldB"].value;
		var c = document.forms["mform"]["ipFieldC"].value;
		var d = document.forms["mform"]["ipFieldD"].value;
		var ip = a + "." + b + "." + c + "." + d;
		document.forms["mform"]["newIpAddress"].value = ip;
		return confirmation();

	}
	function confirmation() {
		var answer = confirm("Are you Sure?");
		if (answer) {
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>


	<%
	if(session.getAttribute("username") == null){
		
		%>
			<jsp:include page="failedMessage.jsp"></jsp:include>
			<%	
	}else{
		Details details = new Details();
		List<HashMap<String, String>> mapListUserandIp = details
				.getAllUsersAndIpAddress();
		List<HashMap<Integer, Integer>> mapList = details
				.getAllUsedIpAddress();
		int index = Integer.parseInt(request.getParameter("index"));

		String oldUsername = mapListUserandIp.get(index).get("name");
		String oldIpAddress = mapListUserandIp.get(index).get("ipAddress");
		int oldIpLastField=Integer.parseInt(oldIpAddress.substring(oldIpAddress.lastIndexOf(".")+1));
		
	%>
	<center>
	<table border =2 cellspacing =0 style="margin:10px; padding:10px; width: auto; height:auto; ">		
						<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;">
			<td>Field</td>
			<td>User Name</td>
			<td>IP Address</td>
			</tr>
			<tr style="color:black; background-color:#CCFFCC ;">
				<td>Old Name</td>
				<td><%=oldUsername%></td>
				<td><%=oldIpAddress%></td>
			</tr>
						<tr style="color:black; background-color:#CCFFCC ;">
			
			<td>New Name</td>
			<form name="mform" method="post" action="modification.jsp" onsubmit="return getIpAddress();">
				
					<td><input type="text" name="newUserName" placeholder="(unchanged)"/></td>
					<td><select name="ipFieldA">
							<option value="192">192</option>
					</select> <select name="ipFieldB">
							<option value="168">168</option>
					</select> <select name="ipFieldC">
							<%
								if (mapList != null) {
									for (int i = 0; i <= 255; i++) {
										boolean shouldShow = true;
										for (HashMap<Integer, Integer> map : mapList) {

											if (map.get(Details.ipFieldC).equals((Integer) i)) {
												shouldShow = false;
												//shouldShow = true;
												break;
											}
										}

										if (!shouldShow) {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
								} else {
									for (int i = 0; i <= 255; i++) {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}
								}
							%>

					</select> </select> <select name="ipFieldD">
							<%
								if (mapList != null) {
									for (int i = 0; i <= 255; i++) {
										boolean shouldShow = true;
										for (HashMap<Integer, Integer> map : mapList) {

											if (map.get(Details.ipFieldD).equals((Integer) i)) {
												if(i != oldIpLastField)
												shouldShow = false;
												//shouldShow = true;
												break;
											}
										}

										if (shouldShow) {
											if(i == oldIpLastField){
												%>
												<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
												<%
											}else{
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}
										}

									}
								} else {
									for (int i = 0; i <= 255; i++) {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}
								}
							%>

					</select>
					</td>
				</tr>
			<tr style="color:black; background-color:#CCFFCC ;">
					<td colspan="3">
					<input type="hidden" name="newIpAddress" />
					<input type="hidden" name="oldIpAddress" value="<%=oldIpAddress%>"/>
					<input type="hidden" name="oldUserName" value="<%=oldUsername %>" />
						<center>
							<input type="submit" value="Change"  />
						</center></td>
				</tr>
				</tr>

			</form>
		</table>
	</center>
	<%
	}
	%>
</body>

</html>