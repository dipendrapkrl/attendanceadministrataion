package com.attendance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlReader {

	
	public static void main(String[] args) {
		XmlReader reader = new XmlReader();
		reader.getIpAndPort();
	}
	public String[] getIpAndPort() {

		String[] ipPort= new String[8];
		  //The two lines below are just for getting an
        //instance of DocumentBuilder which we use
        //for parsing XML data
        DocumentBuilderFactory factory =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
		

        //Here we do the actual parsing
        Document doc;
		
			doc = builder.parse(new File("WebContent/ipaddress.xml"));
		

        
        Element rootElement = doc.getDocumentElement();
       NodeList list = rootElement.getChildNodes();
       
        int index = 0;
        for (int i = 1; i < list.getLength(); i+=2) {
           
            Node childNode = list.item(i);
            System.out.println("data in child number "+
                i+": "+childNode.getTextContent());
            ipPort[index++]=childNode.getTextContent();
        }
    } catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		for (int i = 0; i < ipPort.length; i++) {
			System.out.println(ipPort[i]);
		}
		return ipPort;

}
	
	public void xmlWriter(int[] ipAndPort) {
		
		 DocumentBuilderFactory factory =
		            DocumentBuilderFactory.newInstance();
		        DocumentBuilder builder;
				try {
					builder = factory.newDocumentBuilder();
				

		        // Here instead of parsing an existing document we want to
		        // create a new one.
		        Document testDoc = builder.newDocument();

		        // This creates a new tag named 'testElem' inside
		        // the document and sets its data to 'TestContent'
		        Element el = testDoc.createElement("testElem");
		        el.setTextContent("TestContent");
		        testDoc.appendChild(el);

		        // The XML document we created above is still in memory
		        // so we have to output it to a real file.
		        // In order to do it we first have to create
		        // an instance of DOMSource
		        DOMSource source = new DOMSource(testDoc);

		        // PrintStream will be responsible for writing
		        // the text data to the file
		        PrintStream ps = new PrintStream("test2.xml");
		        StreamResult result = new StreamResult(ps);

		        // Once again we are using a factory of some sort,
		        // this time for getting a Transformer instance,
		        // which we use to output the XML
		        TransformerFactory transformerFactory = TransformerFactory
		            .newInstance();
		        Transformer transformer = transformerFactory.newTransformer();

		        // The actual output to a file goes here
		        transformer.transform(source, result);
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TransformerConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }

}
