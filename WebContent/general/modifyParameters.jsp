<%@page import="java.util.HashMap"%>
<%@page import="com.attendance.Details"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">

function confirmation() {
	var answer = confirm("Are you Sure?");
	if (answer){
		return true;
	}
	else{
		return false;
	}
}


</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Modify System Parameters</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>

	<%
		if (session.getAttribute("username") == null) {
	%>
	<jsp:include page="failedMessage.jsp"></jsp:include>
	<%
		} else {
	%>
	<center>
		<table border=2 cellspacing=0
			style="margin: 10px; padding: 10px; width: auto; height: auto;">
			<%
				boolean hasTriedtoChange = false;
					Details details = new Details();
					if (request.getParameter("loginMinHr") != null) {
						hasTriedtoChange = true;
						//Check for post back
						int startMinHr = Integer.parseInt(request
								.getParameter("loginMinHr"));
						int startMinMin = Integer.parseInt(request
								.getParameter("loginMinMin"));
						int startMaxHr = Integer.parseInt(request
								.getParameter("loginMaxHr"));
						int startMaxMin = Integer.parseInt(request
								.getParameter("loginMaxMin"));

						int endMinHr = Integer.parseInt(request
								.getParameter("logoutMinHr"));
						int endMinMin = Integer.parseInt(request
								.getParameter("logoutMinMin"));
						int endMaxHr = Integer.parseInt(request
								.getParameter("logoutMaxHr"));
						int endMaxMin = Integer.parseInt(request
								.getParameter("logoutMaxMin"));
						int ping = Integer.parseInt(request.getParameter("ping"));

						if (details.setParameters(startMinHr, startMinMin,
								startMaxHr, startMaxMin, endMinHr, endMinMin,
								endMaxHr, endMaxMin, ping)) {
			%>
			<tr style="color: #70CD37; background-color: #339966; font: bold;">
				<td colspan="3"><center>Successfully Changed</center></td>
			</tr>
			<%
				} else {
			%>
			<tr style="color: #E95145; background-color: #339966; font: bold;">
				<td colspan="3"><center>Failed To Change</center></td>
			</tr>
			<%
				}

					}
					HashMap<Integer, Integer> parameters = details
							.getSystemParameters();
					if (parameters == null) {
						out.println("Failed To load parameters this time");

					} else {

					}
			%>





			<form action="modifyParameters.jsp" method="post" onsubmit="return confirmation();">
				<tr style="color: #F3F9F6; background-color: #339966; font: bold;">
					<td>Field</td>
					<td>Min Time</td>
					<td>Max Time</td>
				</tr>

				<tr style="color: black; background-color: #CCFFCC;">
					<td>Login</td>
					<td><select name="loginMinHr">

							<%
								Integer minHr = parameters.get(Details.START_MIN_HOUR);

									for (int i = 0; i <= 23; i++) {
										if (minHr == (Integer) i) {
							%>
							<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
							<%
								} else {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
							%>
					</select> :<select name="loginMinMin">
							<%
								Integer minMin = parameters.get(Details.START_MIN_MINUTE);

									for (int i = 0; i <= 59; i++) {
										if (minMin == (Integer) i) {
							%>
							<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
							<%
								} else {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
							%>
					</select></td>
					<td><select name="loginMaxHr">

							<%
								Integer maxHr = parameters.get(Details.START_MAX_HOUR);

									for (int i = 0; i <= 23; i++) {
										if (maxHr == (Integer) i) {
							%>
							<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
							<%
								} else {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
							%>
					</select> :<select name="loginMaxMin">
							<%
								Integer maxMin = parameters.get(Details.START_MAX_MINUTE);

									for (int i = 0; i <= 59; i++) {
										if (maxMin == (Integer) i) {
							%>
							<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
							<%
								} else {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
							%>
					</select></td>

					<tr style="color: black; background-color: #CCFFCC;"><td>Logout</td>
					<td><select name="logoutMinHr">

							<%
								Integer logoutMinHr = parameters.get(Details.END_MIN_HOUR);

									for (int i = 0; i <= 23; i++) {
										if (logoutMinHr == (Integer) i) {
							%>
							<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
							<%
								} else {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
							%>
					</select> :<select name="logoutMinMin">
							<%
								Integer logoutMinMin = parameters.get(Details.END_MIN_MINUTE);

									for (int i = 0; i <= 59; i++) {
										if (logoutMinMin == (Integer) i) {
							%>
							<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
							<%
								} else {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
							%>
					</select></td>
					<td><select name="logoutMaxHr">

							<%
								Integer logoutMaxHr = parameters.get(Details.END_MAX_HOUR);

									for (int i = 0; i <= 23; i++) {
										if (logoutMaxHr == (Integer) i) {
							%>
							<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
							<%
								} else {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
							%>
					</select> :<select name="logoutMaxMin">
							<%
								Integer logoutMaxMin = parameters.get(Details.END_MAX_MINUTE);

									for (int i = 0; i <= 59; i++) {
										if (logoutMaxMin == (Integer) i) {
							%>
							<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
							<%
								} else {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
								}

									}
							%>
					</select></td>
				</tr>




					<tr style="color: black; background-color: #CCFFCC;"><td>Ping Time</td>
					<td colspan="2">
						<center>
							<select name="ping">
								<%
									Integer ping = parameters.get(Details.PING_TIME_MINUTE);

										for (int i = 0; i <= 120; i++) {
											if (ping == (Integer) i) {
								%>
								<option value="<%=i%>" selected="<%=i%>"><%=i%></option>
								<%
									} else {
								%>
								<option value="<%=i%>"><%=i%></option>
								<%
									}

										}
								%>
							</select>Minute
						</center>
				</tr>



					<tr style="color: black; background-color: #CCFFCC;">
			<td colspan="4"><center>
							<input type=submit
								value="<%if (hasTriedtoChange)
					out.println("Change Again");
				else
					out.println("Change");%>">
						</center></td>
				</tr></form>


		</table>
	</center>
	<%
		}
	%>
</body>
</html>