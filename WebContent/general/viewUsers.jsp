<%@page import="java.util.HashMap"%>
<%@page import="com.attendance.Details"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>

	<%

	
if(session.getAttribute("username") == null){
	
	%>
			<jsp:include page="failedMessage.jsp"></jsp:include>
			<%	
}else{
	int count= 1; 
		Details details = new Details();
		
		List<HashMap<String, String>>mapList = details.getAllUsersAndIpAddress();
	if(mapList == null){
		//Handle accordingly
	}else{
	%>
	<center>
		<table border =2 cellspacing =0 style="margin:10px; padding:10px; width: auto; height:auto; ">
		
				<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;">
			<td>S.N.</td>
				<td>User Name</td>
				<td>User IP address</td>
			</tr>
			<%
	boolean alternateRow= false;
	for(HashMap<String, String>map:mapList){
		
	if(alternateRow){
	%>
				<tr style="color:black; background-color:#CCFFCC ;">
				<%
	}else{
	%>
	<tr style="color:black; background-color:#CCFFCC ;">
	<%
	}
	alternateRow= !alternateRow;
	
				%>
				
				
				<td><%=count++ %></td>
				<td><%=map.get("name") %></td>
				<td><%=map.get("ipAddress") %></td>


			</tr>
			
			<%
	}
			%>
			
		</table>


	</center>

	<%
	
}
}

%>
</body>
</html>