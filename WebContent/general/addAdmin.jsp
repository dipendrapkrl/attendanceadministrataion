<%@page import="com.attendance.Details"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Admin</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>


<center>

<%
boolean alreadyAdded= false;
if(session.getAttribute("username") == null){
	
	%>
			<jsp:include page="failedMessage.jsp"></jsp:include>
			<%	
}else{
%>
	<table border =2 cellspacing =0 style="margin:10px; padding:10px; width: auto; height:auto; ">		
				<%
							String username = request.getParameter("username");
							String password = request.getParameter("password");
							Details details = new Details();
							if (username != null && password != null) {
								if (details.addAdmin(username, password)) {
									alreadyAdded= true;
									%>									
											<tr style="color: #70CD37; background-color: #339966; font: bold;">
											<td colspan="2"><center>Successfully Added</center></td></tr>
																
									<%
									
								} else {
						%> 
	<tr style="color: #E95145; background-color: #339966; font: bold;">
	<td colspan="2"><center>Couldn't Add</center> </td></tr>
	<%
				}
							}
								
	%>
		
		<form method="post" action="addAdmin.jsp">
						<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;">
		<td colspan=2><center>Add Administrator</center></td></tr>
							<tr style="color:black; background-color:#CCFFCC ;">
				<td>Admin Name</td>
				<td><input type="text" name="username" /></td>
			<tr style="color:black; background-color:#CCFFCC ;">
				<td>Password</td>
				<td><input type="password" name="password" /></td>
			<tr style="color:black; background-color:#CCFFCC ;">
				<td colspan="2"><center><input type="submit" value="<%if (alreadyAdded) out.println("Add Another");else out.println("Add"); %>" /></center></td>
			</tr>

		</form>
	</table>
	
	<%} %>
	
	</center>
</body>
</html>