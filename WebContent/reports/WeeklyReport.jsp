<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Attendance</title>

</head>
<body>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>

<%
if(session.getAttribute("username") == null){
	
	%>
			<jsp:include page="../general/failedMessage.jsp"></jsp:include>
			<%
}else{
%>
<center>
	

				<center>
					<form action="Reports.jsp" method="post" id="weeklyReport">
	<table border =2 cellspacing =0 style="margin:10px; padding:10px; width: auto; height:auto; ">	
											<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;">
								<td colspan="2"><center>Generate Report</center></td>
							</tr>
											<tr style="color:black; background-color:#CCFFCC ;">
								<td>Year</td>
								<td><select name="year" style="width: 65px">

										<option value="2011" >2011</option>
										<option value="2012"selected="2012">2012</option>
								</select></td>
							</tr>


											<tr style="color:black; background-color:#CCFFCC ;">
								<td>Month</td>
								<td><select name="month" style="width: 65px">
										<option value="1">Jan</option>
										<option value="2">Feb</option>
										<option value="3">Mar</option>
										<option value="4">Apr</option>
										<option value="5">May</option>
										<option value="6">Jun</option>
										<option value="7" selected="7">Jul</option>
										<option value="8">Aug</option>
										<option value="9">Sept</option>
										<option value="10">Oct</option>
										<option value="11" >Nov</option>
										<option value="12">Dec</option>
								</select></td>
							</tr>

											<tr style="color:black; background-color:#CCFFCC ;">
								<td>Week</td>

								<td><select name="week" style="width: 65px">

										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4" selected="4">4</option>
										<option value="5">5</option>
								</select></td>


							</tr>
											<tr style="color:black; background-color:#CCFFCC ;">
								<td colspan="2"><center>
										<input type=submit value="Retrieve">
									</center></td>
							</tr>
						</table>
					</form>
				
		
	</center>
	<%
}
	%>
</body>
</html>