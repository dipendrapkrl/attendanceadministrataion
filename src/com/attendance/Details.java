package com.attendance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import com.sun.org.apache.bcel.internal.classfile.PMGClass;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import sun.net.util.IPAddressUtil;

public class Details {

	
	public static String message1="";
	public static String message2="";
	public static final String DEFAULT_MESSAGE="";
	public static final Integer START_MIN_HOUR = 1;
	public static final Integer START_MIN_MINUTE = 2;
	public static final Integer START_MAX_HOUR = 3;
	public static final Integer START_MAX_MINUTE = 4;
	public static final Integer END_MIN_HOUR = 5;
	public static final Integer END_MIN_MINUTE = 6;
	public static final Integer END_MAX_HOUR = 7;
	public static final Integer END_MAX_MINUTE = 8;
	public static final Integer PING_TIME_MINUTE = 9;
	
	
	public static final Integer ipFieldA=1;
	public static final Integer ipFieldB=2;
	public static final Integer ipFieldC=3;
	public static final Integer ipFieldD=4;

	private String driver = "com.mysql.jdbc.Driver";
	private String user = "root";
	private String pass = "root";
	private String db = "pingDb";
	private String url = "jdbc:mysql://127.0.0.1:3306/";

	public String[][] logintime ;//= new String[30][8];
	public String[][] logouttime;// = new String[30][8];
	public String[][] status ;//= new String[30][8];
	public String[] userName ;//= new String[30];
	public int[] userId ;//= new int[30];
	public int startDate, endDate;
	public Calendar fullStartDate, fullEndDate;
	public int noOfUsers;
	public int noOfDays;
	public Integer[] weekDays = new Integer[7];
	public List<String>customDays;

	
	public Details(){
		
		/*XmlReader reader = new XmlReader();
		String[] data = reader.getIpAndPort();
		url="jdbc:mysql://"+data[0]+"."+data[1]+"."+data[2]+"."+data[3]+":"+data[4]+"/";
		user=data[5];
		pass= data[6];
		System.out.println(url);*/
	}
	
	
	
	/**
	 * adds the admin to the admin list. the first admin will be super admin by
	 * default
	 * 
	 * @param username
	 * @param password
	 * @return false if user already exists , true if successful
	 */
	public boolean addAdmin(String username, String password) {
		if (username == "" || password == "")
			return false;
		boolean result = false;
		Connection con = null;

		int val = isAnyAdminPresentYet() ? 0 : 1;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "INSERT INTO admins(name, password,is_super_admin) values(?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setInt(3, val);
			ps.executeUpdate();

			result = true;
			con.close();
		} catch (SQLException e) {
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			result = false;
			e.printStackTrace();
		}
		System.out.println(result);
		return result;

	}

	public HashMap<Integer, Integer> getSystemParameters() {

		HashMap<Integer, Integer> parameters = new HashMap<Integer, Integer>();

		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT * FROM info WHERE id in (SELECT min(id) FROM info) ";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				parameters.put(START_MIN_HOUR,
						rs.getInt(rs.findColumn("min_login_time_hr")));
				parameters.put(START_MIN_MINUTE,
						rs.getInt(rs.findColumn("min_login_time_min")));
				parameters.put(START_MAX_HOUR,
						rs.getInt(rs.findColumn("max_login_time_hr")));
				parameters.put(START_MAX_MINUTE,
						rs.getInt(rs.findColumn("max_login_time_min")));
				parameters.put(END_MIN_HOUR,
						rs.getInt(rs.findColumn("min_logout_time_hr")));
				parameters.put(END_MIN_MINUTE,
						rs.getInt(rs.findColumn("min_logout_time_min")));
				parameters.put(END_MAX_HOUR,
						rs.getInt(rs.findColumn("max_logout_time_hr")));
				parameters.put(END_MAX_MINUTE,
						rs.getInt(rs.findColumn("max_logout_time_min")));
				parameters.put(PING_TIME_MINUTE,
						rs.getInt(rs.findColumn("ping")));
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return parameters;

	}

	public boolean setParameters(int startMinHr, int startMinMin,
			int startMaxHr, int startMaxMin, int endMinHr, int endMinMin,
			int endMaxHr, int endMaxMin, int ping) {
		if (!isValidHr(startMinHr))
			return false;
		if (!isValidHr(startMaxHr))
			return false;
		if (!isValidHr(endMaxHr))
			return false;
		if (!isValidHr(endMinHr))
			return false;
		if (!isValidMin(startMinMin))
			return false;
		if (!isValidMin(startMaxMin))
			return false;
		if (!isValidMin(endMinMin))
			return false;
		if (!isValidMin(endMaxMin))
			return false;

		
		
		String sql_select = "select min(id) from info";
		String sql = "update info set min_login_time_hr =?, min_login_time_min=?, max_login_time_hr=?,max_login_time_min=?, min_logout_time_hr=?,min_logout_time_min=?,max_logout_time_hr=?, max_logout_time_min=?, ping = ? where id =?";
		Connection con = null;
		boolean result = false;
		try {
			int id=1 ;
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			PreparedStatement pres = con.prepareStatement(sql_select);
			ResultSet set = pres.executeQuery();
			while(set.next()){
				id = set.getInt(1);
			}
			
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, startMinHr);
			ps.setInt(2, startMinMin);
			ps.setInt(3, startMaxHr);
			ps.setInt(4, startMaxMin);
			ps.setInt(5, endMinHr);
			ps.setInt(6, endMinMin);
			ps.setInt(7, endMaxHr);
			ps.setInt(8, endMaxMin);
			ps.setInt(9, ping);
			ps.setInt(10, id);

			int affected = ps.executeUpdate();

			con.close();
			if (affected > 0)
				result = true;
		} catch (SQLException e) {
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			result = false;
			e.printStackTrace();
		}
		return result;

	}

	private boolean isValidMin(int min) {
		if (min < 0 || min > 59) {
			return false;

		} else
			return true;

	}

	private boolean isValidHr(int hr) {
		if (hr < 0 || hr > 24) {
			return false;

		} else
			return true;
	}

	public boolean isUserAlreadyPresent(String name) {

		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT * FROM users where name = ? ";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = true;
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}

	public boolean isIpAlreadyPresent(String ip) {

		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT * FROM users where ip_address = ? ";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, ip);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = true;
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}

	public boolean addUser(String name, String ipAddress) {
		boolean result = false;
		Connection con = null;

		if (isUserAlreadyPresent(name))
			return false;
		if (isIpAlreadyPresent(ipAddress))
			return false;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "INSERT INTO users(name, ip_address)values(?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, ipAddress);
			ps.executeUpdate();

			result = true;
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		}
		return result;

	}

	/**
	 * 
	 * @param ipAddress
	 *            array of ip address of the users who should be removed
	 * @return true if successful false otherwise
	 */
	public boolean removeUsers(String[] ipAddress) {

		for (String string : ipAddress) {
			System.out.println(string);
		}
		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			for (int i = 0; i < ipAddress.length; i++) {
				System.out.println(ipAddress[i]);
				// String sql =
				// "DELETE FROM attendence WHERE users_id IN (SELECT id FROM users WHERE name=?)";
				String sql = "DELETE FROM attendence WHERE users_id IN (SELECT id FROM users WHERE ip_address=?)";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, ipAddress[i]);
				ps.executeUpdate();
				// String sql2 = "DELETE FROM users WHERE name = ?";
				String sql2 = "DELETE FROM users WHERE ip_address = ?";
				PreparedStatement ps2 = con.prepareStatement(sql2);
				ps2.setString(1, ipAddress[i]);
				int y = ps2.executeUpdate();
				System.out.println("affected =" + y);
				if (y > 0)
					result = true;

			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		}
		return result;

	}

	public boolean modifyUserName(String oldName, String newName) {
		boolean result = false;
		Connection con = null;

		if (isUserAlreadyPresent(newName)){
			System.out.println("Already Present");
			return false;
		}
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "UPDATE users SET name=? where name = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, newName);
			ps.setString(2, oldName);
			int x = ps.executeUpdate();
			if (x > 0)
				result = true;

			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		}
		return result;

	}

	public boolean modifyIpAddress(String oldIp, String newIp) {
		boolean result = false;
		Connection con = null;

		if (isIpAlreadyPresent(newIp))
			return false;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "UPDATE users SET ip_address=? where ip_address = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, newIp);
			ps.setString(2, oldIp);
			int x = ps.executeUpdate();
			if (x > 0)
				result = true;

			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		}
		return result;

	}

	/**
	 * 
	 * @param fieldHour
	 *            hour field to be updated
	 * @param fieldMin
	 *            min field to be updated
	 * @param hour
	 *            value for hour field
	 * @param min
	 *            value for min field
	 * @return true if successful else false
	 */
	public boolean updateOfficeTimeParameter(String fieldHour, String fieldMin,
			int hour, int min) {

		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "UPDATE INFO SET ?=? AND ?=? WHERE id =1";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, fieldHour);
			ps.setInt(2, hour);
			ps.setString(3, fieldMin);
			ps.setInt(4, min);
			ps.executeUpdate();

			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		}
		return result;
	}

	public boolean changePassword(String username, String oldPass,
			String newPass) {
		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "UPDATE admins set password = ? where password = ? and name = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, newPass);
			ps.setString(2, oldPass);
			ps.setString(3, username);
			int affected = ps.executeUpdate();
			System.out.println(affected);
			if (affected >= 1)
				result = true;

			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		}
		return result;
	}

	public boolean isSuperAdmin(String admin) {
		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT * FROM admins where is_super_admin=1";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = true;

			} else {
				result = false;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		}
		return result;
	}

	public boolean removeAdmin(String removerAdmin, String[] removeeAdminId) {

		if (!isSuperAdmin(removerAdmin)) {
			System.out.println(false);
			return false;
		}

		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);

			for (String string : removeeAdminId) {

				String sql = "DELETE FROM admins where id = ? ";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, string);

				ps.executeUpdate();
			}

			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		}
		System.out.println(result);
		return result;

	}

	/**
	 * used to check if there is at least one admin is present or not
	 * 
	 * @return true if at least one admin is present
	 */
	public boolean isAnyAdminPresentYet() {

		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT * FROM admins";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = true;
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}

	/**
	 * access name using "name" and ip address using "ipAddress"
	 * 
	 * @return name, ip_address pair of all users present in the database
	 *         returns null if failed
	 * 
	 */
	public List<HashMap<String, String>> getAllUsersAndIpAddress() {

		List<HashMap<String, String>> mapList = new ArrayList<HashMap<String, String>>();

		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT name,ip_address FROM users";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("name", rs.getString(rs.findColumn("name")));
				map.put("ipAddress", rs.getString(rs.findColumn("ip_address")));
				mapList.add(map);
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mapList = null;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mapList = null;
		}
		return mapList;

	}
	public List<HashMap<Integer, Integer>> getAllUsedIpAddress() {
		
		List<HashMap<Integer, Integer>> mapList = new ArrayList<HashMap<Integer, Integer>>();
		
		Connection con = null;
		
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT ip_address FROM users";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				
				
				
				HashMap<Integer	, Integer> map = new HashMap<Integer, Integer>();
				//String ipAddress = rs.getString(rs.findColumn("ip_address"));
				String ipAddress = rs.getString(1);
				//String ipAddes = rs.getString(rs.f)
				
				
				if(IPAddressUtil.isIPv4LiteralAddress(ipAddress)){
				
				StringTokenizer tokenizer = new StringTokenizer(ipAddress, ".");
				
				if(tokenizer.hasMoreTokens()){
				
				map.put(ipFieldA, Integer.decode(tokenizer.nextToken()));
				map.put(ipFieldB, Integer.decode(tokenizer.nextToken()));
				map.put(ipFieldC, Integer.decode(tokenizer.nextToken()));
				map.put(ipFieldD, Integer.decode(tokenizer.nextToken()));
				
				
				mapList.add(map);
				}
			}
			}
			
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mapList = null;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mapList = null;
		}
		
		for (int i = 0; i <= 255; i++) {
			boolean shouldShow = true;
			for (HashMap<Integer, Integer> map : mapList) {

				if (map.get(Details.ipFieldD).equals((Integer) i)) {
					shouldShow = false;
					break;
				}
			}

			if (!shouldShow) {
				System.out.println(i);
			}
		}
			
		return mapList;
		
	}

	/**
	 * 
	 * @return id, name,of all admins present in the database returns null if
	 *         failed
	 */
	public java.util.List<String[]> getAllAdmins() {

		java.util.List<String[]> map = new ArrayList<String[]>();
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT id, name FROM admins";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String[] adminInfo = new String[2];
				adminInfo[0] = rs.getString(rs.findColumn("id"));
				adminInfo[1] = rs.getString(rs.findColumn("name"));
				map.add(adminInfo);
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			map = null;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			map = null;
		}
		return map;

	}

	/**
	 * checks whether the user name password pair is correct or not
	 * 
	 * @param username
	 * @param password
	 * @return username if correct else null is returned
	 */
	public boolean isUserPassCorrect(String username, String password) {

		boolean result = false;
		Connection con = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT * FROM admins where name = ? and password = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = true;
				con.close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}

	public void setWeekDays() {
		int i = fullStartDate.get(Calendar.DATE);
		for (int a = 0; a < 7; a++) {
			if (i > fullEndDate.get(Calendar.DATE))
				break;
			weekDays[a] = new Integer(i); // to set date coz day starts with 0 not 1

			i++;
			// System.out.println(days[a]);
		}
		noOfDays=7;
	}
	public void setCustomDays() {
		
		int count=0;
		customDays = new ArrayList<String>();
		//Calendar strtDate = fullStartDate;
		Calendar strtDate = Calendar.getInstance();
		strtDate.set(fullStartDate.get(Calendar.YEAR), fullStartDate.get(Calendar.MONTH), fullStartDate.get(Calendar.DATE));
		
		boolean shouldStop= fullEndDate.get(Calendar.MONTH)==strtDate.get(Calendar.MONTH)
				&&fullEndDate.get(Calendar.DATE)==strtDate.get(Calendar.DATE)
				&&fullEndDate.get(Calendar.YEAR)==strtDate.get(Calendar.YEAR);
		while(!shouldStop ){
			System.out.println(strtDate.get(Calendar.MONTH)+"-"+strtDate.get(Calendar.DATE));
			shouldStop= fullEndDate.get(Calendar.MONTH)==strtDate.get(Calendar.MONTH)
					&&fullEndDate.get(Calendar.DATE)==strtDate.get(Calendar.DATE)
					&&fullEndDate.get(Calendar.YEAR)==strtDate.get(Calendar.YEAR);
			
			customDays.add((strtDate.get(Calendar.MONTH)+1)+"-"+strtDate.get(Calendar.DATE));
			strtDate.set(Calendar.DATE, strtDate.get(Calendar.DATE)+1);
			
			count++;
			
		}
		noOfDays=count;
		System.out.println("After setting custom records");
		
		
	}

	public boolean doesExistsRecord(int week, int month, int year) {
		setStartAndEndDate(week, month, year); //
		try {
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url + db, user, pass);
			// String tempStart=
			String stDate = String.valueOf(fullStartDate.get(Calendar.YEAR))
					+ ":"
					+ String.valueOf(fullStartDate.get(Calendar.MONTH) + 1)
					+ ":" + String.valueOf(fullStartDate.get(Calendar.DATE));
			String enDate = String.valueOf(fullEndDate.get(Calendar.YEAR))
					+ ":" + String.valueOf(fullEndDate.get(Calendar.MONTH) + 1)
					+ ":" + String.valueOf(fullEndDate.get(Calendar.DATE));
			String sql = "SELECT * FROM attendence WHERE attendence_date  BETWEEN ? AND ?";

			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, stDate);
			pre.setString(2, enDate);
			ResultSet rs = pre.executeQuery();
			if (rs.next()) {
				con.close();
				return true;
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
	public boolean doesExistsRecord(int yearStart, int monthStart, int dayStart, int yearEnd, int monthEnd, int dayEnd) {
		boolean result= false;
		setStartAndEndDate(yearStart,monthStart,dayStart,yearEnd, monthEnd, dayEnd);
		try {
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url + db, user, pass);
			String stDate = String.valueOf(fullStartDate.get(Calendar.YEAR))
					+ ":"
					+ String.valueOf(fullStartDate.get(Calendar.MONTH) + 1)
					+ ":" + String.valueOf(fullStartDate.get(Calendar.DATE));
			String enDate = String.valueOf(fullEndDate.get(Calendar.YEAR))
					+ ":" + String.valueOf(fullEndDate.get(Calendar.MONTH) + 1)
					+ ":" + String.valueOf(fullEndDate.get(Calendar.DATE));
			String sql = "SELECT * FROM attendence WHERE attendence_date  BETWEEN ? AND ?";
			
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, stDate);
			pre.setString(2, enDate);
			ResultSet rs = pre.executeQuery();
			System.out.println(rs.getStatement());
			if (rs.next()) {
				con.close();
				System.out.println("should have result");
				result= true;
			}else{
				System.out.println("No records");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = false;
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result= false;
		}
		
		return result;
	}

	public void setUserIdAndName() {
		try {
			Class.forName(driver);

			Connection con = DriverManager.getConnection(url + db, user, pass);
			String sql = "SELECT id ,name FROM users";
			PreparedStatement pre = con.prepareStatement(sql);
			ResultSet rs = pre.executeQuery();
			int i = 0;
			while (rs.next()) {
				userId[i] = rs.getInt("id");
				userName[i] = rs.getString("name");
				i++;
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("here is error");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setNoOfUsers() {
		noOfUsers = -1;
		// System.out.println("no of users =" + noOfUsers);
		try {
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "select COUNT(ip_address) from users";
			PreparedStatement prest = con.prepareStatement(sql);
			ResultSet rs = prest.executeQuery();
			while (rs.next()) {
				noOfUsers = rs.getInt("COUNT(ip_address)");

			}
			con.close();
		} catch (ClassNotFoundException e) {
			System.out.println("class not found");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Sql exception");
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.out.println("NUll pointer exception");
			e.printStackTrace();
		}
		System.out.println("no of users returned = " + noOfUsers);

	}

	public void setStartAndEndDate(int week, int month, int year) {
		// this function should set the start and end date of the desired week

		if (week == 1) {
			startDate = 1;
			endDate = 7;
		} else if (week == 2) {
			startDate = 8;
			endDate = 14;
		} else if (week == 3) {
			startDate = 15;
			endDate = 21;
		} else if (week == 4) {
			startDate = 22;
			endDate = 28;
		} else if (week == 5) {
			startDate = 29;
			endDate = 31;
		}

		fullStartDate = Calendar.getInstance();
		fullStartDate.clear();
		fullStartDate.set(year, month, startDate);

		fullEndDate = Calendar.getInstance();
		fullEndDate.clear();
		if (week == 5) {

			endDate = fullStartDate.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		fullEndDate.set(year, month, endDate);

	}
	public void setStartAndEndDate(int yearStart, int monthStart, int dayStart, int yearEnd, int monthEnd, int dayEnd) {
		// this function should set the start and end date of the desired week
		
		
		fullStartDate = Calendar.getInstance();
		fullStartDate.clear();
		fullStartDate.set(yearStart, monthStart, dayStart);
		
		fullEndDate = Calendar.getInstance();
		fullEndDate.clear();
		 
		fullEndDate.set(yearEnd, monthEnd, dayEnd);
		System.out.println("Start and end date set to "+fullStartDate.get(Calendar.YEAR)
				
				+fullStartDate.get(Calendar.MONTH)+fullStartDate.get(Calendar.DATE)
				+"  "+fullEndDate.get(Calendar.YEAR)+fullEndDate.get(Calendar.MONTH)+fullEndDate.get(Calendar.DATE));
	}
 
	public void getReports() {

		
		String date;
		int b = 0, a = 0; // to decrease time for next loop. Remember,
							// getDate(i) function changes its value every time
							// it is being called except getDate(0)
		try {
			Class.forName(driver);
			Connection con = null;

			con = DriverManager.getConnection(url + db, user, pass);
			for (int i = 0; i < noOfUsers; i++) {
				getDate(-b); // since -0 and 0 are same so no effect for first
								// loop

				for (int j = 0; j < noOfDays; j++) {

					String sql = "SELECT  u.name, u.id, a.login_time,a.logout_time,a.status FROM users u, attendence a WHERE u.id=a.users_id AND u.id = ? AND a.attendence_date=?";
					PreparedStatement pre = con.prepareStatement(sql);

					pre.setInt(1, userId[i]);

					if (j == 0) {
						date = getDate(0);

					} else {
						date = getDate(1);
						a += 1;
					}

					if (date == null) // null is returned only when the last
										// week have less than 7 days
						continue;

					pre.setString(2, date);
					System.out.println("***********" + date + "**************");
					System.out.println("id : " + userId[i]);

					ResultSet rs = pre.executeQuery();
					while (rs.next()) {

						logintime[i][j] = rs.getString("login_time");
						logouttime[i][j] = rs.getString("logout_time");
						status[i][j] = rs.getString("status");

						System.out.println("User " + rs.getString("name"));
						System.out.println("login time " + logintime[i][j]);
						System.out.println("logout time " + logouttime[i][j]);
						System.out.println("status " + status[i][j]);
						System.out.println("************************************");
					}

				}
				b = a;
				a = 0;

			}
			System.out.println("just at end of try block of get reports");
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void getCustomReports() {
		
		System.out.println("Inside custom report");
		

		System.out.println(fullStartDate.get(Calendar.MONTH)+" "+fullStartDate.get(Calendar.DATE));
		System.out.println(fullEndDate.get(Calendar.MONTH)+" "+fullEndDate.get(Calendar.DATE));
		
		
		String date;
		int b = 0, a = 0; // to decrease time for next loop. Remember,
		// getDate(i) function changes its value every time
		// it is being called except getDate(0)
		try {
			Class.forName(driver);
			Connection con = null;
			
			con = DriverManager.getConnection(url + db, user, pass);
			// System.out.println("no of users  "+noOfUsers);
			for (int i = 0; i < noOfUsers; i++) {
				getDate(-b); // since -0 and 0 are same so no effect for first
				// loop
				for (int j = 0; j < noOfDays; j++) {
					
					String sql = "SELECT  u.name, u.id, a.login_time,a.logout_time,a.status FROM users u, attendence a WHERE u.id=a.users_id AND u.id = ? AND a.attendence_date=?";
					PreparedStatement pre = con.prepareStatement(sql);
					
					pre.setInt(1, userId[i]);
					
					if (j == 0) {
						date = getDate(0);
						
					} else {
						date = getDate(1);
						a += 1;
					}
					
					if (date == null) // null is returned only when the last
						// week have less than 7 days
						continue;
					
					pre.setString(2, date);
					System.out.println("***********" + date + "**************");
					System.out.println("id : " + userId[i]);
					
					ResultSet rs = pre.executeQuery();
					// System.out.println(a++);
					while (rs.next()) {
						
						logintime[i][j] = rs.getString("login_time");
						logouttime[i][j] = rs.getString("logout_time");
						status[i][j] = rs.getString("status");
						
					}
					
				}
				b = a;
				a = 0;
				
			}
			System.out.println("just at end of try block of get reports");
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int k = 0; k < noOfUsers; k++) {
			for (int k2 = 0; k2 < customDays.size(); k2++) {
				System.out.println("login time " + logintime[k][k2]);
				System.out.println("logout time " + logouttime[k][k2]);
				System.out.println("status " + status[k][k2]);
			}
		}
		System.out.println("end of custom report");
	}

	public String getDate(int j) {
		// tempDate.clear();
		String temp;
		Calendar tempDate = null;
		// tempDate.clear();String
		if (j == 0) {
			tempDate = fullStartDate;
			temp = tempDate.get(Calendar.YEAR) + "-";
			int tempnext = tempDate.get(Calendar.MONTH) + 1; // adjusting date
																// before
																// sending it to
																// database
			temp += +tempnext + "-" + (tempDate.get(Calendar.DATE));

			// + tempDate.get(Calendar.MONTH) + "-"
			// + tempDate.get(Calendar.DATE);
			// System.out.println(temp);
			return temp;
		} else {
			tempDate = fullStartDate;

			tempDate.add(Calendar.DATE, j);
			temp = tempDate.get(Calendar.YEAR) + "-";
			int tempnext = tempDate.get(Calendar.MONTH) + 1; // adjusting date
																// before
																// sending it to
																// database
			temp += "-" + tempnext + "-" + (tempDate.get(Calendar.DATE));

			if (tempDate.after(fullEndDate)) {
				// System.out.println(temp);
				return null;
			} else {
				// System.out.println(temp);
				return temp;
			}
		}

	}
	
	public void setWeekAllValues(){
		setNoOfUsers(); //stores the no. of users in noOfUsers

		//details.setStartAndEndDate(week, month, year); //sets the beginning and ending of the week no need ad doesexistrecord function calls it 
		setWeekDays(); //stores the date of the given week
		setAllRequiredParameters();

		setUserIdAndName(); //stores array of user id
		getReports();
	}
	public void setCustomAllValues(){
		
		setNoOfUsers(); //stores the no. of users in noOfUsers
		//details.setStartAndEndDate(week, month, year); //sets the beginning and ending of the week no need ad doesexistrecord function calls it 
		setCustomDays(); //stores the date of the given week
		setAllRequiredParameters();
		setUserIdAndName(); //stores array of user id
		getCustomReports();
		//getReports();
	}

	private void setAllRequiredParameters() {
		// TODO Auto-generated method stub
		
		 logintime =new String[noOfUsers][noOfDays];//= new String[30][8];
			logouttime=new String[noOfUsers][noOfDays];// = new String[30][8];
			status =new String[noOfUsers][noOfDays];//= new String[30][8];
			userName =new String[noOfUsers];//= new String[30];
			userId =new int[noOfUsers];//= new int[30];
			
	}

}
