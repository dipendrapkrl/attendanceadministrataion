<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.attendance.Details,java.*,javax.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Weekly Attendance</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>


	<%
	if(session.getAttribute("username") == null){
		
		%>
				<jsp:include page="../general/failedMessage.jsp"></jsp:include>
				<%	
	}else{
			 String yrStart = request.getParameter("yearStart");
			 String yrEnd = request.getParameter("yearEnd");
			 String mthStart = request.getParameter("monthStart");
			 String mthEnd = request.getParameter("monthEnd");
			 String dyStart = request.getParameter("dayStart");
			 String dyEnd = request.getParameter("dayEnd");
			 int yearStart = Integer.parseInt(yrStart);
			 int yearEnd = Integer.parseInt(yrEnd);
			 int monthStart = Integer.parseInt(mthStart)-1;
			 int monthEnd = Integer.parseInt(mthEnd)-1;
			 int dayStart = Integer.parseInt(dyStart);
			 int dayEnd = Integer.parseInt(dyEnd);

			

			Details details = new Details();
			if(details.doesExistsRecord(yearStart, monthStart, dayStart, yearEnd, monthEnd, dayEnd)==false){
		out.println("No records  of specified time in the database <br>");
			} else {

		details.setCustomAllValues();

	%>

	<center>
		<h1 style="color: #F3F9F6; background-color: #339966; font: bold;">
			Attendance From<br>
			<%=yearStart+"-"+(monthStart+1)+"-"+dayStart+" To "+yearEnd+"-"+(monthEnd+1)+"-"+dayEnd%>
			</h1>
		<table border=2 cellspacing=0
			style="margin: 10px; padding: 10px; width: auto; height: auto;table-layout: fixed;">

			<tr style="color: #F3F9F6; background-color: #339966; font: bold;">
				<td>Name</td>
				<td>Info</td>
				<%
					//to display date in the first column
					
				
					List<String> days = details.customDays;
					for(String string : days){
							 		
				%>
				<td style="width=100px;"><center><%=string%></center></td>
				<%
					}
				%>


			</tr>

			<%
				for (int i = 0; i < details.noOfUsers; i++) {

						if (i % 2 == 0) {
			%>
			<tr style="color:black; background-color:#CCFFCC ;">
				<%
					} else {
				%>
			
			<tr style="background-color: gray;">
				<%
					}
							for (int j = -2; j < details.customDays.size() ; j++) { //use j<check instead   //j is initialized to -1 to fit the table(table contains extra column in the left)
								//if(j==3)break;			//j<check-1 coz we should decrease because of above reason
								if (j == -2) {
				%>
				<td><%=details.userName[i]%></td>
				<%
					//since 0th term of array stores  the name of the respective user 
									continue;

								} else if (j == -1) {
				%>
				<td><center>IN<br>OUT</center>
				</td>
				<%
					continue;
								}

								if (details.status[i][j] == "0"
										|| details.status[i][j] == null) {
				%>
				<td><%="<center>_</center>"%></td>
				<%
					} else {
				%>
				<td>
					<%
						String loginValue, logoutValue;
										if (details.logintime[i][j] == null
												&& details.logouttime[i][j] == null) {
											loginValue = "-";
										} else {
											if (details.logintime[i][j] == null)
												loginValue = "<center>_</center>";
											else
												loginValue = details.logintime[i][j];
											if (details.logouttime[i][j] == null)
												logoutValue = "<center>_</center>";
											else
												logoutValue = details.logouttime[i][j];
					%> <%=loginValue%> <%="\n"%> <br> <%=logoutValue%>
				</td>
				<%
					}

								}
							}
				%>
			</tr>
			<%
				}
				}
	}
			%>



		</table>
		
	</center>

</body>
</html>