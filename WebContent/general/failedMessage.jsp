<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Oops, Something Weird Happened</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>

	<center>
		<h1 style="color: #F3F9F6; background-color: #339966; font: bold;">
			Sorry, We Couldnt Identify You.<br> Please <a
				href="../index.jsp">Login</a> To Gain Access.<br> Thankyou!
		</h1>
	</center>
</body>
</html>