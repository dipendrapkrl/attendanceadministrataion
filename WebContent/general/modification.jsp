<%@page import="com.attendance.Details"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Modify</title>
</head>
<body>
	<center>
		<h1 style="color: #F3F9F6; background-color: #339966; font: bold;">
			Automatic Attendance System</h1>
	</center>

	<%
		if (session.getAttribute("username") == null) {
	%>
	<jsp:include page="failedMessage.jsp"></jsp:include>
	<%
		} else {

			String oldUserName = request.getParameter("oldUserName");
			String newUserName = request.getParameter("newUserName");
			String oldIpAddress = request.getParameter("oldIpAddress");
			String newIpAddress = request.getParameter("newIpAddress");
			boolean IpAltered = !oldIpAddress.equalsIgnoreCase(newIpAddress);
			if(newUserName=="")newUserName=oldUserName;
			boolean nameAltered = !oldUserName.equals(newUserName);


			boolean isNameModificationSuccesful = false;
			boolean isIPModificationSuccessful = false;
			Details details = new Details();

			if (IpAltered && nameAltered) {
				isNameModificationSuccesful = details.modifyUserName(
						oldUserName, newUserName);
				isIPModificationSuccessful = details.modifyIpAddress(
						oldIpAddress, newIpAddress);
				if (!(isNameModificationSuccesful && isIPModificationSuccessful)) {
					if (!isNameModificationSuccesful) {
						details.modifyUserName(newUserName, oldUserName);
					} else {
						details.modifyIpAddress(newIpAddress, oldIpAddress);
					}
						Details.message1 = "Modification Of Both Username And IP Address Failed6";
				}else{
					Details.message1="Modification Successful";
				}

			} else if (nameAltered) {
				isNameModificationSuccesful = details.modifyUserName(
						oldUserName, newUserName);
				if (isNameModificationSuccesful) {
					Details.message1 = "Modification of Name Successful";

				} else
					Details.message1 = "Modification Of Name Failed";
			}else if (IpAltered) {
				isIPModificationSuccessful = details.modifyIpAddress(
						oldIpAddress, newIpAddress);
				if (isIPModificationSuccessful)
					Details.message1 = "Modification Successful";

				else
					Details.message1 = "Modification Of IP Address Failed";
			} else{
				Details.message1="Nothing Was Changed";
			}
			String redirectURL = "../welcome.jsp";
			response.sendRedirect(redirectURL);

		}
	%>

</body>
</html>