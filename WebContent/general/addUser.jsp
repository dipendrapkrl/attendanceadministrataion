<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.attendance.Details"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add User</title>

<script type="text/javascript">
	function getIpAddress() {

		var a = document.forms["mform"]["ipFieldA"].value;
		var b = document.forms["mform"]["ipFieldB"].value;
		var c = document.forms["mform"]["ipFieldC"].value;
		var d = document.forms["mform"]["ipFieldD"].value;
		var ip = a + "." + b + "." + c + "." + d;
		document.forms["mform"]["ipAddress"].value = ip;
		return true;

	}
</script>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>

	<center>
		<table border=2 cellspacing=0
			style="margin: 10px; padding: 10px; width: auto; height: auto;">
			<%
				boolean alreadyAdded = false;
				if (session.getAttribute("username") == null) {
			%>
			<jsp:include page="failedMessage.jsp"></jsp:include>
			<%
				} else {
					String name = request.getParameter("name");
					String ipAddress = request.getParameter("ipAddress");
					Details details = new Details();
					List<HashMap<Integer, Integer>> mapList = details
							.getAllUsedIpAddress();

					if (name != null || ipAddress != null) {
						if (details.addUser(name, ipAddress)) {
			%>
							<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;">
				<td colspan="2"><center>
						<%
							alreadyAdded = true;
										out.println("Successfully Added");
						%>
					</center></td>
			</tr>
			<%
				} else {
			%>
			<tr>
				<td colspan="2"><center>
						<%
							out.println("Failed To Add");
						%>
					</center></td>
			</tr>
			<%
				}
					}
			%>

			<form method="post" name="mform" action="addUser.jsp"
				onsubmit="return getIpAddress();">
				<tr style="color: #F3F9F6; background-color: #339966; font: bold;">

					<td colspan="2"><center>Add User</center></td>
				</tr>
							<tr style="color:black; background-color:#CCFFCC ;">

				<td>Name</td>
				<td><input type="text" name="name" /></td>
			</tr>
							<tr style="color:black; background-color:#CCFFCC ;">

				<td>IP Address</td>
				<td><select name="ipFieldA">
						<option value="192">192</option>
				</select> <select name="ipFieldB">
						<option value="168">168</option>
				</select> <select name="ipFieldC">
						<%
							if (mapList != null) {
									for (int i = 0; i <= 255; i++) {
										boolean shouldShow = true;
										for (HashMap<Integer, Integer> map : mapList) {

											if (map.get(Details.ipFieldC).equals((Integer) i)) {
												shouldShow = false;
												break;
											}
										}

										if (!shouldShow) {
						%>
						<option value="<%=i%>"><%=i%></option>
						<%
							}

									}
								} else {
									for (int i = 0; i <= 255; i++) {
						%>
						<option value="<%=i%>"><%=i%></option>
						<%
							}
								}
						%>

				</select> </select> <select name="ipFieldD">
						<%
							if (mapList != null) {
									for (int i = 0; i <= 255; i++) {
										boolean shouldShow = true;
										for (HashMap<Integer, Integer> map : mapList) {

											if (map.get(Details.ipFieldD).equals((Integer) i)) {
												shouldShow = false;
												break;
											}
										}

										if (shouldShow) {
						%>
						<option value="<%=i%>"><%=i%></option>
						<%
							}

									}
								} else {
									for (int i = 0; i <= 255; i++) {
						%>
						<option value="<%=i%>"><%=i%></option>
						<%
							}
								}
						%>

				</select> <!-- <option value="192">192</option> </select> </select></td> -->
			</tr>
							<tr style="color:black; background-color:#CCFFCC ;">

				<td colspan="2"><input type="hidden" name="ipAddress" />
					<center>
						<input type="submit"
							value="<%if (alreadyAdded)
					out.println("Add Another");
				else
					out.println("Add");%>" />
					</center></td>
			</tr>
			</form>
		</table>
	</center>
	<%
		}
	%>
</body>
</html>