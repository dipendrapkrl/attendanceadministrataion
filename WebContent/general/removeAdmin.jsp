<%@page import="java.util.HashMap"%>
<%@page import="com.attendance.Details"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">

function confirmation() {
	var answer = confirm("Are you Sure?");
	if (answer){
		return true;
	}
	else{
		return false;
	}
}


</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Remove Administrator</title>
</head>
<body>
	<center><h1 style="color: #F3F9F6; background-color: #339966; font: bold;"> Automatic Attendance System</h1></center>

<center>
			<table border =2 cellspacing =0 style="margin:10px; padding:10px; width: auto; height:auto; ">
	<%

boolean alreadyRemoved= false;
if(session.getAttribute("username") == null){
	
	%>
			<jsp:include page="failedMessage.jsp"></jsp:include>
			<%	
}else{
		Details details = new Details();
		
		String removerAdmin = session.getAttribute("username").toString();
		String[] adminInfo = request.getParameterValues("adminId");
		//int adminId = Integer.parseInt(adminInfo[0]);
		if(adminInfo !=null){
			
			
			boolean result = details.removeAdmin(removerAdmin, adminInfo);
				alreadyRemoved = true;
			if(result){
				%>									
				<tr style="color: #70CD37; background-color: #339966; font: bold;"><td colspan="2"><center>Successfully Removed</center></td></tr>
												
					<%
			}else{
				%>									
				<tr style="color: #E95145; background-color: #339966; font: bold;"><td colspan="2"><center>Failed to Remove</center></td></tr>
												
					<%
			}
		}
		List<String[]>mapList = details.getAllAdmins();
	if(mapList == null){
		//Handle accordinglyt
	}else{
	%>
	
						<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;"><td colspan = 3><center>Select To Remove Administrators</center></td></tr>
		<form method="post" action = "removeAdmin.jsp" onsubmit="return confirmation();" >
							<tr style="color:#F3F9F6; background-color:#339966 ;font: bold;">
			<td>Select</td>
				<td>Admin Name</td>
				
			</tr>
			<%
	
	for(String[] map:mapList){
		
	
	%>
							<tr style="color:black; background-color:#CCFFCC ;">				
				<td><input type="checkbox" name="adminId"
					value="<%=map[0]%>"/></td>
				<td><%=map[1] %></td>
				
			</tr>
			
			<%
	}
			%>
							<tr style="color:black; background-color:#CCFFCC ;"><td colspan = "3"><center><input type= "submit" value = "<%if(alreadyRemoved)out.println("Remove Again");else out.println("Remove"); %>" /></center></td></tr>

</form>
		</table>


	</center>

	<%
	}
}
	

%>
</body>
</html>